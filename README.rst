=========================================================
Semi-automatic building of upstream trees for NIC testing
=========================================================

Setup
=====
Save the Red Hat LNST Jenkins host name into ``~/lnst-jenkins.hostname``.

Login to the Red Hat LNST Jenkins web interface. In your profile Configuration,
create an API Token and save it in ``~/.netrc``::

  machine <jenkins-host-name>
  login <user-name>
  password <Jenkins-API-token>

Run the ``initialize.sh`` script to prepare the *working directory*
``~/auto-build``. The working directory location is hardcoded.

Configuration
=============
Under the working directory, each ``configs/*.conf`` file describes a kernel
git tree to pull, how to build it and what NIC drivers to test the resulting
build on. The conf files consist of ``<key>=<value>`` assignments (suitable for
sourcing from bash). Not all values have to be assigned in each conf file.
Default values are set in ``defaults.conf``.

Available keys:

* ``remote=``
  Name of the git remote to fetch.

* ``branch=``
  Branch in the git remote to check out.

* ``config=``
  Name of a file in the working directory to use as the base ``.config`` for
  building the kernel. It does not have to match the checked out kernel source
  exactly, as ``make olddefconfig`` will be applied before building.

* ``suffix=``
  Suffix to add to the RPM release field. Note that it will be
  always followed by the short git hash of the checked out tree.

* ``disttag=``
  Dist tag to end the RPM release field with.

* ``target=``
  Brew target to build in.
  
* ``gittagprefix=``
  Prefix of the git tag to add when tagging the source tree after submitting it
  to Brew for building.

* ``drivers=``
  A comma-separated list of names of NIC drivers to test the build on. 

Building kernels
================
You need to be able to connect to Brew and have a valid Kerberos ticket.

Run the ``auto-build.sh`` script. It fetches the configured remote branches.
If there are no updates in a branch since the last build, it skips the branch.
It applies the kernel config, trims some fat, creates a source RPM and sends it
to Brew.

In the current implementation, this script processes all conf files once and
exits.

Testing the kernels
===================
You need to be able to connect to Jenkins and have configured the API Token.

Run the ``auto-test.sh`` script. It watches the Brew builds created by
``auto-build.sh`` and submits the successful ones to Jenkins.

``auto-test.sh`` loops indefinitely.

Submitting a single Brew build to test
======================================
``auto-test.sh`` uses a helper script to submit Brew builds to Jenkins.
The script can be used standalone as well. If you have already built a kernel
in Brew, you can ask the LNST Jenkins to test it with:

``lnst-jenkins-trigger.sh <Brew-task-ID> <NIC-driver>[,<NIC-driver>...]``

Trimming fat
============
``trim-fat.sh`` is a script to make the build quicker and the RPMs smaller by
disabling and/or deleting debuginfo, graphics, sound, wireless drivers,
non-x86_64 architecture code. It is called by ``auto-build.sh``, but it can be
used independently as well. You have to run ``make olddefconfig`` afterwards
to restore the concistency of ``.config``.
