#!/bin/sh
set -ex

DEST=~/auto-build

mkdir -p "$DEST/state/"
(cd "$DEST/state" && mkdir -p heads tmp building testing failed)
cp defaults.conf "$DEST"
cp -r configs/ "$DEST"
cp kernel-5.14.0-x86_64-debug.config kernel-5.14.0-x86_64-rt-debug.config "$DEST"

cd "$DEST"
git clone --reference-if-able ~/git/kernel --dissociate git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
cd linux
git remote add net-next git://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git
git remote add net git://git.kernel.org/pub/scm/linux/kernel/git/davem/net.git
git remote add tnguy-next git://git.kernel.org/pub/scm/linux/kernel/git/tnguy/next-queue.git
git remote add tnguy-net git://git.kernel.org/pub/scm/linux/kernel/git/tnguy/net-queue.git
git remote add linux-next git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
git remote add linux-rt-devel git://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git
