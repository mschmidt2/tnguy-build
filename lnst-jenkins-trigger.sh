#!/bin/bash

if [ $# != 2 ]; then
	echo "Usage: $0 <task_id> <net_drivers>"
	echo "  task_id     - Brew task id number"
	echo "  net_drivers - comma-separated list of NIC drivers"
	exit 1
fi

JENKINS="$(cat ~/lnst-jenkins.hostname)"
JOB="job/LNST-BREW-kernel-net-driver"

curl -s -i --netrc -X POST -F BREW_TASKIDS="$1" -F KERNEL_NETDRIVER="$2" -F CATALOG_WORKFLOW="upstream" -F TEST_GROUP="cnb-functional" "https://$JENKINS/$JOB/buildWithParameters" | \
	sed 's/\r$//' | \
{
	read http_result
	[ "$http_result" = "HTTP/1.1 201 Created" ] || exit 1
	while read line; do
		regex='^location: .*/queue/item/([0-9]+)/$'
		if [[ "$line" =~ $regex ]]; then
			queueid=${BASH_REMATCH[1]}
			echo $queueid
			exit 0
		fi
	done
}

## Example output of buildWithParameters:
# HTTP/1.1 201 Created
# ...
# location: https://<jenkins-host>/queue/item/3948/
# ..
#
## The queue item number can be used to find the job and its result:
# curl -g -X GET 'https://<jenkins-host>/job/LNST-BREW-kernel-net-driver/api/xml?tree=builds[id,number,result,queueId]&xpath=//build[queueId=3948]'
# <build _class="org.jenkinsci.plugins.workflow.job.WorkflowRun"><id>196</id><number>196</number><queueId>3948</queueId><result>FAILURE</result></build> 
#
## Example query of more than one queueId (using "or" and the "wrapper" argument):
# curl -g -X GET "https://$(cat ~/lnst-jenkins.hostname)/job/LNST-BREW-kernel-net-driver/api/xml?tree=builds[id,result,queueId]&xpath=//build[queueId=4020%20or%20queueId=4023]&wrapper=root"
